<?php
//Code goes here
add_action( 'wp_enqueue_scripts', 'resellnow_enqueue_styles',PHP_INT_MAX );
function resellnow_enqueue_styles() {
    $parenthandle = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.
    $theme = wp_get_theme();
    wp_enqueue_style( $parenthandle, get_template_directory_uri() . '/style.css', 
        array(),  // if the parent theme code has a dependency, copy it to here
        $theme->parent()->get('Version')
    );
    wp_enqueue_style( 'child-style', get_stylesheet_uri(),
        array( $parenthandle ),$theme->parent()->get('Version')
    );
}

add_action( 'show_user_profile', 'customer_income' );
add_action( 'edit_user_profile', 'customer_income' );

function customer_income( $user ) { ?>
    <h3><?php _e("Extra profile information", "blank"); ?></h3>

    <table class="form-table">
    <tr>
        <th><label for="customer_income"><?php _e("Customer Income"); ?></label></th>
        <td>
            <input type="text" name="customer_income" id="customer_income" value="<?php echo esc_attr( get_the_author_meta( 'customer_income', $user->ID ) ); ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your customer income."); ?></span>
        </td>
    </tr>
   </table><?php 
}

add_action( 'personal_options_update', 'save_customer_income' );
add_action( 'edit_user_profile_update', 'save_customer_income' );

function save_customer_income( $user_id ) {
    if ( !current_user_can( 'customer_income', $user_id ) ) { 
        return false; 
    }
    update_user_meta( $user_id, 'customer_income',  $_POST['customer_income'] );
}

//shop name
add_action( 'show_user_profile', 'shopName' );
add_action( 'edit_user_profile', 'shopName' );

function shopName( $user ) { ?>

    <table class="form-table">
    <tr>
        <th><label for="shopName"><?php _e("Shop Name"); ?></label></th>
        <td>
            <input type="text" name="shopname" id="shopName" value="<?php echo esc_attr( get_the_author_meta( 'shopname', $user->ID ) ); ?>" class="regular-text" />
        </td>
    </tr>
   </table><?php 
}

add_action( 'personal_options_update', 'save_shopName' );
add_action( 'edit_user_profile_update', 'save_shopName' );

function save_shopName( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) { 
        return false; 
    }
    update_user_meta( $user_id, 'shopname', sanitize_text_field( $_POST['shopname']) );
} 

// Bkash number
add_action( 'show_user_profile', 'bkash' );
add_action( 'edit_user_profile', 'bkash' );

function bkash( $user ) { ?>

    <table class="form-table">
    <tr>
        <th><label for="bkash"><?php _e("bKash Account"); ?></label></th>
        <td>
            <input type="text" name="bkash" id="bkash" value="<?php echo esc_attr( get_the_author_meta( 'bkash', $user->ID ) ); ?>" class="regular-text" /><br />
        </td>
    </tr>
   </table><?php 
}

add_action( 'personal_options_update', 'save_bkash' );
add_action( 'edit_user_profile_update', 'save_bkash' );

function save_bkash( $user_id ) {
    if ( !current_user_can( 'edit_user', $user_id ) ) { 
        return false; 
    }
    update_user_meta( $user_id, 'bkash', $_POST['bkash']) ;
}

/**********************************
 *  Save field value
 ***********************************/
//add_action( 'woocommerce_save_account_details', 'save_bkash_number' );
//function save_bkash( $user_id ) {
//    if ( !current_user_can( 'edit_user', $user_id ) ) { 
//        return false; 
//    }
//    update_user_meta( $user_id, 'bkash', sanitize_text_field( $_POST['bkash'] );
//}
//add_action( 'woocommerce_save_account_details', 'save_shop_name' );
//function save_shop_name( $user_id ) {
//    if ( !current_user_can( 'edit_user', $user_id ) ) { 
//        return false; 
//    }
//    update_user_meta( $user_id, 'shopname', sanitize_text_field( $_POST['shopname'] );
//} 

                     
 /**********************************
 *  Make woocommerce  Last name optional
 ***********************************/
 add_filter('woocommerce_save_account_details_required_fields', 'resellnow_required_fields');

function resellnow_required_fields( $required_fields ) {
	unset( $required_fields['account_last_name'] );
	// unset( $account_fields['account_first_name'] ); // First name
	// unset( $account_fields['account_display_name'] ); // Display name
	return $required_fields;
}