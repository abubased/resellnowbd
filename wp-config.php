<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'unaux_26072134_demo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'aSSebw_sGISBbe`HqejP`zg  I^nyrmA?lQ&4;KXnl4wYwucs [B~A3>?~jL*!!+' );
define( 'SECURE_AUTH_KEY',  'dj[=t._;pqaTn{t~SwGbp|xS9pZk/vkav^,fh(jiaVCG~Wv)|RqB[XXWS`uFMH1T' );
define( 'LOGGED_IN_KEY',    ')?XY:)~k}O1b):Bbq`uc=j(w#f0hR%YlF]3rbc5M X<2D)U!ZE,P/j])>9D~Lg(J' );
define( 'NONCE_KEY',        'QeQzSxzE~C*qNQAdK]Eb@=7$*02b6r^LQ*u+pg;o%H+<}6$+..ru09ZA@2HSR,$[' );
define( 'AUTH_SALT',        'U0/=w)l =3=(!i_]gzU>)#r]3V6l0kgUAYkgr?n3|gZr]w;e0z6&L[?^Iv3V}{,T' );
define( 'SECURE_AUTH_SALT', '3!G7S(r/rague`=?%R{R}dsxEz,bCw!*(57~PR0z/44]mFLIZ_6#/vl`~3u*ev[G' );
define( 'LOGGED_IN_SALT',   's6rZGeVIzJn;AZ3~LLyz!S:mB/yz^Lf%f[T>^;MfW2u`$ a%l|]~`)G.>=j>B!B?' );
define( 'NONCE_SALT',       'Ktd_ZuHSmhw`Lzc_0u<m!C:P12rZ?IzmDsh%d}vD&/cxR.:FYC7-x=b^dgP2!OHX' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'multivendor_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
